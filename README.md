# ansible-laptop

My own laptop setup with Ansible

Easy and simple setup, currently on `Ubuntu 18.04` to just
* install packages
* do some basic configuration

To run `ansible` first of all prepare the environment

```
virtualenv venv
source venv/bin/activate
pip install ansible
```

Then just run

```
ansible-playbook -vvv hosts/localhost.yml
```